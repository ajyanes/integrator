package com.aia.integrator.payment.repository;

import com.aia.integrator.payment.domain.Payment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends PaymentRepositoryCustom, CrudRepository<Payment, Long> {

}
