package com.aia.integrator.payment.repository.impl;

import com.aia.integrator.costcenter.domain.CostCenter;
import com.aia.integrator.payment.domain.Payment;
import com.aia.integrator.payment.repository.PaymentRepositoryCustom;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

@Repository
@PropertySource("classpath:sentences.xml")
public class PaymentRepositoryCustomImpl implements PaymentRepositoryCustom {

  @Autowired
  private EntityManager em;

  @Autowired
  private Environment env;


  @Override
  public List<Payment> getListPayment(StringBuilder param) {
    String query = env.getProperty("showPayment.sql");
    query += param;
    Query nativeQuery = em.createNativeQuery(query, Payment.class);
    nativeQuery.setHint("org.hibernate.fetchSize",1000);

    return nativeQuery.getResultList();
  }
}
