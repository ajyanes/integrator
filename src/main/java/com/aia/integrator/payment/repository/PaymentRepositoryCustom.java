package com.aia.integrator.payment.repository;

import com.aia.integrator.payment.domain.Payment;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepositoryCustom {

  List<Payment> getListPayment(StringBuilder param);

}
