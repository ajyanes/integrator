package com.aia.integrator.payment.service;

import com.aia.integrator.payment.domain.Payment;
import java.util.List;

public interface PaymentService {

  List<Payment> getPayments(String nit);

}
