package com.aia.integrator.payment.service.impl;

import com.aia.integrator.payment.domain.Payment;
import com.aia.integrator.payment.repository.PaymentRepository;
import com.aia.integrator.payment.service.PaymentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentServiceImpl implements PaymentService {

  @Autowired
  private PaymentRepository paymentRepository;

  @Override
  public List<Payment> getPayments(String nit) {
    StringBuilder query = new StringBuilder();

    if (!nit.equals("")) {
      query.append(" and coptercero = '" + nit+"'");
    }

    return paymentRepository.getListPayment(query);
  }
}
