package com.aia.integrator.payment.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import lombok.Data;

@Data
@Entity
@IdClass(value = PaymentPk.class)
public class Payment implements Serializable {

  @Id
  @Column(name = "nit_pagador")
  private String nit_pagador;

  @Column(name = "razon_social")
  private String razon_social;

  @Column(name = "fecha_pago")
  private Date fecha_pago;

  @Column(name = "cuenta_pagador")
  private String cuenta_pagador;

  @Column(name = "tipo_cuenta_pagador")
  private String tipo_cuenta_pagador;

  @Column(name = "banco_pagador")
  private String banco_pagador;

  @Column(name = "CuentaBancaria")
  private String cuentaBancaria;

  @Column(name = "modalidad_pago")
  private String modalidad_pago;

  @Id
  @Column(name = "numero_egreso")
  private int numero_egreso;

  @Column(name = "correo")
  private String correo;

  @Column(name = "comentario")
  private String comentario;

  @Column(name = "valor")
  private String valor;

  @Column(name = "compania")
  private String compañia;

  @Column(name = "numero_factura")
  private String numero_factura;

  @Column(name = "numero_prefijo")
  private String numero_prefijo;

  @Column(name = "imp_totiva")
  private String imp_totiva;

  @Column(name = "imp_reteiva")
  private String imp_reteiva;

  @Column(name = "imp_impucons")
  private String imp_impucons;

  @Column(name = "imp_totalimpue")
  private String imp_totalimpue;

  @Column(name = "imp_tototroimpu")
  private String imp_tototroimpu;

  @Column(name = "imp_tototrarete")
  private String imp_tototrarete;

  @Column(name = "imp_totretotrimp")
  private String imp_totretotrimp;

  @Column(name = "imp_totalreten")
  private String imp_totalreten;

  @Column(name = "imp_totalica")
  private String imp_totalica;
}
