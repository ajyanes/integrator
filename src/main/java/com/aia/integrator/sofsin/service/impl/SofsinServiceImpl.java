package com.aia.integrator.sofsin.service.impl;

import com.aia.integrator.sofsin.service.SofsinService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

@Service
public class SofsinServiceImpl implements SofsinService {

    private static final String REGISTRAR_PROCESO = "SP_REGISTRAR_PROCESO";
    private static final String PROCESAR_IMPORTACION = "SP_PROCESAR_IMPORTACION";
    private static final String COMPANIA = "as_cia";
    private static final String USUARIO = "as_usuario";
    private static final String PARAMETRO = "as_parametros";
    private static final String ORIGEN = "as_origen";
    private static final String IDENTIFICADOR = "an_identificador";
    private static final String ERROR = "as_mensajeerror";
    private static final String GET_IDENTIFICADOR = "an_identificador";

    @Value("${spring.datasource.driverClassName}")
    private String driverClass;
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String user;
    @Value("${spring.datasource.password}")
    private String pass;

    @Override
    public BigDecimal getIdentifier(String company, String user, String param, String src) {
        DriverManagerDataSource ds = getDataSource();
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(ds)
                .withProcedureName(REGISTRAR_PROCESO);

        SqlParameterSource in = new MapSqlParameterSource().addValue(COMPANIA, company)
                .addValue(USUARIO, user).addValue(PARAMETRO, param)
                .addValue(ORIGEN, src).addValue(IDENTIFICADOR,0)
                .addValue(ERROR, "");

        Map<String, Object> out = jdbcCall.execute(in);

        return (BigDecimal) out.get(GET_IDENTIFICADOR);

    }

    @Override
    public void processImportation(String company, BigDecimal identifier) {
        DriverManagerDataSource ds = getDataSource();
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(ds)
            .withProcedureName(PROCESAR_IMPORTACION);

        SqlParameterSource in = new MapSqlParameterSource().addValue(COMPANIA, company)
            .addValue(IDENTIFICADOR, identifier).addValue(ERROR, "");

        jdbcCall.execute(in);
    }

    private DriverManagerDataSource getDataSource() {
        DriverManagerDataSource datasource = new DriverManagerDataSource();
        datasource.setDriverClassName(driverClass);
        datasource.setUrl(url);
        datasource.setUsername(user);
        datasource.setPassword(pass);

        return datasource;
    }

}
