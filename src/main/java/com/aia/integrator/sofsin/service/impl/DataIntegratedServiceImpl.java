package com.aia.integrator.sofsin.service.impl;

import com.aia.integrator.sofsin.domain.DataIntegrated;
import com.aia.integrator.sofsin.repository.DataIntegratedRepository;
import com.aia.integrator.sofsin.service.DataIntegratedService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataIntegratedServiceImpl implements DataIntegratedService {

  @Autowired
  private DataIntegratedRepository dataIntegratedRepository;

  @Override
  public List<DataIntegrated> getInvoicesIntegrated(String nume, String prove, String tipodoc, String compania) {
    StringBuilder qry = new StringBuilder();

    if(!compania.equals("")){
      qry.append(" and edpcompania = '"+compania+"'");
    }

    if(!nume.equals("")){
      qry.append(" and edpnumero = '"+nume+"'");
    }

    if(!prove.equals("")){
      qry.append(" and edpproveedor = '"+prove+"'");
    }

    if(!tipodoc.equals("")){
      qry.append(" and edptipodocu = '"+tipodoc+"'");
    }

    return dataIntegratedRepository.getInvoiceIntegrated(qry);
  }

}
