package com.aia.integrator.sofsin.service;

import com.aia.integrator.sofsin.domain.ResponseIntegration;
import com.aia.integrator.sofsin.domain.SosfinWrapper;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface SosfsinWrapperService {
    List<ResponseIntegration> saveSofsinWrapper(List<SosfinWrapper> sosfinWrapperList);


}
