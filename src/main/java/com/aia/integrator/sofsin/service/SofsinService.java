package com.aia.integrator.sofsin.service;

import java.math.BigDecimal;

public interface SofsinService {

    BigDecimal getIdentifier(String company, String user, String param, String src);

    void processImportation(String company, BigDecimal identifier);
}
