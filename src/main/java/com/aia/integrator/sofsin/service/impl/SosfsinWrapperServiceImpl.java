package com.aia.integrator.sofsin.service.impl;

import com.aia.integrator.sofsin.domain.ResponseIntegration;
import com.aia.integrator.sofsin.domain.SofsinHeader;
import com.aia.integrator.sofsin.domain.SosfinWrapper;
import com.aia.integrator.sofsin.repository.SofsinDetailRepository;
import com.aia.integrator.sofsin.repository.SofsinHeaderRepository;
import com.aia.integrator.sofsin.service.SofsinService;
import com.aia.integrator.sofsin.service.SosfsinWrapperService;
import com.aia.integrator.util.Constant;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

@Service
public class SosfsinWrapperServiceImpl implements SosfsinWrapperService {

    private SofsinDetailRepository sofsinDetailRepository;
    private SofsinHeaderRepository sofsinHeaderRepository;
    private SofsinService sofsinService;

    @Autowired
    public SosfsinWrapperServiceImpl(SofsinHeaderRepository sofsinHeaderRepository, SofsinService sofsinService, SofsinDetailRepository sofsinDetailRepository) {
        this.sofsinDetailRepository = sofsinDetailRepository;
        this.sofsinHeaderRepository = sofsinHeaderRepository;
        this.sofsinService = sofsinService;
    }

    public SosfinWrapper saveSofsinWrapper(SosfinWrapper sosfinWrapper) {
        BigDecimal identifier = setIdentifierToSofsinHeader(sosfinWrapper);
        setControlToSosfinWrapper(sosfinWrapper);
        try {
            sofsinHeaderRepository.save(sosfinWrapper.getSofsinHeader());
            sofsinDetailRepository.saveAll(sosfinWrapper.getSofsinDetails());
            sofsinService.processImportation(Constant.CONSCOMPANY, identifier);
        } catch (Exception e) {
            sosfinWrapper.getSofsinHeader().setEdpMessage(e.getMessage());
        }
        return sosfinWrapper;
    }

    private void setControlToSosfinWrapper(SosfinWrapper sosfinWrapper) {
        String control = sosfinWrapper.getSofsinHeader().getEdpControl();
        sosfinWrapper.getSofsinHeader().setEdpControl(control);
        sosfinWrapper.getSofsinDetails().forEach(sofsinDetail -> sofsinDetail.setDdpControl(control));
    }

    private BigDecimal setIdentifierToSofsinHeader(SosfinWrapper sosfinWrapper) {
        BigDecimal identifier = sofsinService.getIdentifier(Constant.CONSCOMPANY, Constant.CONSUSER, Constant.CONSPARAM, Constant.CONSSRC);
        sosfinWrapper.getSofsinHeader().setEdpIdentificador(identifier.longValue());
        return identifier;
    }

    private String getControl(SofsinHeader sofsinHeader) {
        LocalDate localDate = LocalDate.now();
        StringBuilder builder = new StringBuilder();
        builder.append(sofsinHeader.getEdpClaseDocum());
        builder.append(localDate.getYear());
        builder.append(localDate.getMonthValue());
        builder.append(sofsinHeader.getEdpNumero());
        return builder.toString();
    }

    public List<ResponseIntegration> saveSofsinWrapper(List<SosfinWrapper> sosfinWrapperList) {
        List<ResponseIntegration> integrations =  new ArrayList();

        for(SosfinWrapper sosfinWrapper : sosfinWrapperList){
            SosfinWrapper wrapper = saveSofsinWrapper(sosfinWrapper);

            ResponseIntegration responseIntegration =new ResponseIntegration();
            responseIntegration.setDocumentNumber(wrapper.getSofsinHeader().getEdpNumero());
            responseIntegration.setDocumentType(wrapper.getSofsinHeader().getEdpTipoDocu());
            responseIntegration.setMessage(wrapper.getSofsinHeader().getEdpMessage().equals("") ? "Registro Exitoso" : "Hubo un error al insertar los datos en la tabla");

            integrations.add(responseIntegration);
        }

        return integrations;
    }
}
