package com.aia.integrator.sofsin.service;

import com.aia.integrator.sofsin.domain.DataIntegrated;
import java.util.List;

public interface DataIntegratedService {

  List<DataIntegrated> getInvoicesIntegrated(String nume, String prov, String tipodoc, String compania);

}
