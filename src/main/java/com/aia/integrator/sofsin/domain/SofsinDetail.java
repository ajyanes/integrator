package com.aia.integrator.sofsin.domain;

import javax.persistence.IdClass;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "CP_DETDOCPAGOENTRA")
@IdClass(value = SofsinDetailPk.class)
public class SofsinDetail implements Serializable {

    @Column(name = "DDPTIPOCONS")
    private String ddpTipoCons;

    @Column(name = "DDPCONCEPCXP")
    private String ddpConcepcxp;

    @Column(name = "DDPPRECIOUNITABRUTO")
    private Long ddpPrecioUnitaBruto;
    @Id
    @Column(name = "DDPCONTROL")
    private String ddpControl;
    @Id
    @Column(name = "DDPCOMPANIA")
    private String ddpCompania;

    @Column(name = "DDPSECUENCIA")
    private Long ddpSecuencia;

    @Column(name = "DDPUNIDAVENTA")
    private String ddpUnidaVenta;

    @Column(name = "DDPCANTIDAD")
    private Long ddpCantidad;

    @Column(name = "DDPTERCERO")
    private String ddpTercero;

    @Column(name = "DDPBASEPRONPAGO")
    private String ddpBasePronPago;
    @Id
    @Column(name = "DDPCONSEINGRE")
    private Long ddpConseIngre;

    @Column(name = "DDPCONSECUTIVO")
    private String ddpConsecutivo;

}
