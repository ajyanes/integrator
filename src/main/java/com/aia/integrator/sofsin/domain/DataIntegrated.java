package com.aia.integrator.sofsin.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import lombok.Data;

@Data
@Entity
@IdClass(value = DataIntegratedPk.class)
public class DataIntegrated implements Serializable {

  @Id
  @Column(name = "edptipocons")
  private String tipocons;
  @Id
  @Column(name = "edpconsecutivo")
  private String consecutivo;
  @Id
  @Column(name = "edpcompania")
  private String compania;

  @Column(name = "edpdivision")
  private String division;

  @Column(name = "edptipodocu")
  private String tipoDocu;

  @Id
  @Column(name = "edpnumero")
  private String numero;

  @Column(name = "edpproveedor")
  private String proveedor;

  @Column(name = "edptipoprove")
  private String tipoProve;

  @Column(name = "edpconcdocu")
  private String concepto;

  @Column(name = "edpcentrorespo")
  private String centroResp;

  @Column(name = "edptotmonedlocal")
  private BigDecimal total;

  @Column(name = "eobcodigo")
  private String codEstado;

  @Column(name = "eobnombre")
  private String estado;

  @Column(name = "edpprefijo")
  private String prefijo;

  @Column(name = "edpfechrece")
  private String fechaRecepcion;

  @Column(name = "edpcomencorto")
  private String observacion;
}
