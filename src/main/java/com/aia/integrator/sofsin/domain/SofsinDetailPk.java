package com.aia.integrator.sofsin.domain;

import java.io.Serializable;

public class SofsinDetailPk implements Serializable {

  private Long ddpConseIngre;
  private String ddpControl;
  private String ddpCompania;
}
