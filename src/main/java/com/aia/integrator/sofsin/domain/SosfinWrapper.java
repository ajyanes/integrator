package com.aia.integrator.sofsin.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SosfinWrapper {
    private SofsinHeader sofsinHeader;
    private List<SofsinDetail> sofsinDetails;
}
