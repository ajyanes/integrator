package com.aia.integrator.sofsin.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "CP_ENCDOCPAGOENTRA")
public class SofsinHeader implements Serializable {

    @Id
    @Column(name = "EDPNUMERO")
    private String edpNumero;

    @Column(name = "EDPCLASEDOCUM")
    private String edpClaseDocum;

    @Column(name = "EDPTIPODOCU")
    private String edpTipoDocu;

    @Column(name = "EDPTIPOCONS")
    private String edpTiCons;

    @Column(name = "EDPTIPOPROVE")
    private String edpTipoProve;

    @Column(name = "EDPCONCDOCU")
    private String edpConcDocu;

    @Column(name = "EDPPROVEEDOR")
    private String edpProveedor;

    @Column(name = "EDPVALORCONTR")
    private Long edpValorContr;

    @Column(name = "EDPTOTMONEDLOCAL")
    private Long edpTotMoneLocal;

    @Column(name = "EDPCENTRORESPO")
    private String edpCentroRespo;

    @Column(name = "EDPPREFIJO")
    private String edpPrefijo;

    @Column(name = "EDPFECHEXPE")
    private Date edpFechExpe;

    @Column(name = "EDPFECHRECE")
    private Date edpFechRece;

    @Column(name = "EDPFECHVENC")
    private Date edpFechVenc;

    @Column(name = "EDPFECHACONTA")
    private Date edpFechaConta;

    @Column(name = "EDPCONTROL")
    private String edpControl;

    @Column(name = "EDPDIVISION")
    private String edpDivision;

    @Column(name = "EDPCOMPANIA")
    private String edpCompania;

    @Column(name = "EDPESTABLECIMIENTO")
    private String edpEstablecimiento;

    @Column(name = "EDPMONEDA")
    private String edpMoneda;

    @Column(name = "EDPFORMAPAGO")
    private String edpFormaPago;

    @Column(name = "EDPCONSEINGRE")
    private int edpConseIngre;

    @Column(name = "EDPCONSECUTIVO")
    private String edpConsecutivo;

    @Column(name = "EDPCOMENCORTO")
    private String edpComenCorto;

    @Column(name = "EDPCOMENTARIO")
    private String edpComentario;

    @Column(name = "EDPIDENTIFICADOR")
    private Long edpIdentificador;

    @Column(name = "EDPVALORAJUSPESO")
    private Long edpValoraJusPeso;

    @Column(name = "EDPMENSAJE")
    private String edpMessage;

    @Column(name = "EDPLOTE")
    private String edpLote;

    @Column(name = "EOBUSUARIO")
    private String user;

}
