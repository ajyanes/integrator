package com.aia.integrator.sofsin.domain;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class Invoice {

  private SofsinHeader sofsinHeader;
  private List<SofsinDetail> sofsinDetails;

}
