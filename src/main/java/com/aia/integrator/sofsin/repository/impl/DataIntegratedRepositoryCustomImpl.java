package com.aia.integrator.sofsin.repository.impl;

import com.aia.integrator.sofsin.domain.DataIntegrated;
import com.aia.integrator.sofsin.repository.DataIntegratedRepositoryCustom;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

@Repository
@PropertySource("classpath:sentences.xml")
public class DataIntegratedRepositoryCustomImpl implements DataIntegratedRepositoryCustom {

  @Autowired
  private EntityManager em;

  @Autowired
  private Environment env;

  @Override
  public List<DataIntegrated> getInvoiceIntegrated(StringBuilder qry) {
    String query = env.getProperty("showDataIntegrated.sql");
    query +=qry;
    Query nativeQuery = em.createNativeQuery(query, DataIntegrated.class);

    return nativeQuery.getResultList();
  }
}
