package com.aia.integrator.sofsin.repository;

import com.aia.integrator.sofsin.domain.DataIntegrated;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface DataIntegratedRepositoryCustom {

  List<DataIntegrated> getInvoiceIntegrated(StringBuilder qry);

}
