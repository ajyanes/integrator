package com.aia.integrator.sofsin.repository;

import com.aia.integrator.sofsin.domain.DataIntegrated;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataIntegratedRepository extends DataIntegratedRepositoryCustom, CrudRepository<DataIntegrated, Long> {

}
