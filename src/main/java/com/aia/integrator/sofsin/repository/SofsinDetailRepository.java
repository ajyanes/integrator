package com.aia.integrator.sofsin.repository;

import com.aia.integrator.sofsin.domain.SofsinDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SofsinDetailRepository extends JpaRepository<SofsinDetail, Long> {

}
