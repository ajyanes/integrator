package com.aia.integrator.sofsin.repository;

import com.aia.integrator.sofsin.domain.SofsinHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface SofsinHeaderRepository extends JpaRepository<SofsinHeader, Long> {

    @Procedure("SP_REGISTRAR_PROCESO")
    Map<String, Object> getRegisterProcess(String company, String user, String parameter, String src);

}
