package com.aia.integrator.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constant {

  public static final String CONSCOMPANY = "01";
  public static final String CONSUSER = "SAIA";
  public static final String CONSPARAM = "|Liberar=S|";
  public static final String CONSSRC = "CXP";

}
