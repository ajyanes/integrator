package com.aia.integrator.controller;


import com.aia.integrator.costcenter.domain.CostCenter;
import com.aia.integrator.costcenter.service.CostCenterService;
import com.aia.integrator.payment.domain.Payment;
import com.aia.integrator.payment.service.PaymentService;
import com.aia.integrator.provider.domain.Provider;
import com.aia.integrator.provider.service.ProviderService;
import com.aia.integrator.sofsin.domain.DataIntegrated;
import com.aia.integrator.rejected.domain.RejectedBill;
import com.aia.integrator.rejected.service.RejectedBillService;
import com.aia.integrator.sofsin.service.DataIntegratedService;
import com.aia.integrator.sofsin.service.SofsinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/integrator")
public class IntegratorController {

    @Autowired
    private CostCenterService costCenterService;

    @Autowired
    private ProviderService providerService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private DataIntegratedService dataIntegratedService;

    @Autowired
    private SofsinService sofsinService;

    @Autowired
    private RejectedBillService rejectedBillService;

    @GetMapping("/costcenter")
    public ResponseEntity<List<CostCenter>> getCostCenter(@RequestParam("cod") String cod, @RequestParam("name") String name, @RequestParam("company") String company) {
        if(cod == "" && name == "" && company == ""){
            String result = "Debe indicar por lo menos un criterio de consulta";
            return new ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<>(costCenterService.getCostCenter(cod, name, company), HttpStatus.ACCEPTED);
        }
    }

    @GetMapping("/provider")
    public ResponseEntity<List<Provider>> getProvider(@RequestParam("cod") String cod, @RequestParam("name") String name, @RequestParam("company") String company) {
        if(cod == "" && name == "" && company == ""){
            String result = "Debe indicar por lo menos un criterio de consulta";
            return new ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<>(providerService.getProvider(cod, name, company), HttpStatus.ACCEPTED);
        }
    }

    @GetMapping("/payment")
    public ResponseEntity<List<Payment>> getPayment(@RequestParam("nit") String nit){
        if(nit == ""){
            String result = "Debe indicar un nit para realizar la consulta";
            return new ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<>(paymentService.getPayments(nit), HttpStatus.ACCEPTED);
        }
    }

    @GetMapping("/integrated")
    public ResponseEntity<List<DataIntegrated>> getIntegrated(@RequestParam("numero") String nume,@RequestParam("proveedor") String prove,
        @RequestParam("tipodoc") String tipodoc, @RequestParam("compania") String compania) {

        if(nume == "" && prove == "" && tipodoc == "" && compania == ""){
            String result = "Debe indicar por lo menos un criterio de consulta";
            return new ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<>(dataIntegratedService.getInvoicesIntegrated(nume, prove,tipodoc,compania), HttpStatus.ACCEPTED);
        }
    }

    @PutMapping("/rejectedInvoice")
    public ResponseEntity<RejectedBill> sendInvoice(@RequestBody RejectedBill rejectedBill) {
        rejectedBillService.saveRejected(rejectedBill);
        return new ResponseEntity<>(rejectedBill, HttpStatus.ACCEPTED);
    }

    @GetMapping("showRejected")
    public ResponseEntity<List<RejectedBill>> getRejected(@RequestParam("factura") String bill, @RequestParam("nit") String provider, @RequestParam("estado") String state) {
        if(bill == "" && provider == "" && state == ""){
            String result = "Debe indicar por lo menos un criterio de consulta";
            return new ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<>(rejectedBillService.getRejected(bill, provider, state), HttpStatus.ACCEPTED);
        }
    }
}
