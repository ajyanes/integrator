package com.aia.integrator.controller;

import com.aia.integrator.sofsin.domain.ResponseIntegration;
import com.aia.integrator.sofsin.domain.SosfinWrapper;
import com.aia.integrator.sofsin.service.SosfsinWrapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/integrator")
public class SosfinWrapperController {

    @Autowired
    private SosfsinWrapperService sosfsinWrapperService;

    @PutMapping("/sofsinIntegration")
    public ResponseEntity<List<ResponseIntegration>> sendInvoice(@RequestBody List<SosfinWrapper> sosfinWrapperList) {
        return  new ResponseEntity<>(sosfsinWrapperService.saveSofsinWrapper(sosfinWrapperList),
            HttpStatus.OK);
    }
}
