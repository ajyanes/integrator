package com.aia.integrator.costcenter.service;

import com.aia.integrator.costcenter.domain.CostCenter;

import java.util.List;

public interface CostCenterService {

    List<CostCenter> getCostCenter(String cod, String name,String company);
}
