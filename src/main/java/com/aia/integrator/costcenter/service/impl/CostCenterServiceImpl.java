package com.aia.integrator.costcenter.service.impl;

import com.aia.integrator.costcenter.domain.CostCenter;
import com.aia.integrator.costcenter.repository.CostCenterRepository;
import com.aia.integrator.costcenter.service.CostCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CostCenterServiceImpl implements CostCenterService {

    @Autowired
    private CostCenterRepository costCenterRepository;

    @Override
    public List<CostCenter> getCostCenter(String cod, String name, String company) {
        StringBuilder query = new StringBuilder();

        if (!company.equals("")) {
            query.append(" and igecompania = '" + company+"'");
        }

        if (!cod.equals("")) {
            query.append(" and (CHARINDEX('"+cod+"',igecodigo,1) > 0 or CHARINDEX('"+cod+"',igenomblarg,1) > 0)");
        }

        if (!name.equals("")) {
            query.append(" and (CHARINDEX('"+name+"',igecodigo,1) > 0 or CHARINDEX('"+name+"',igenomblarg,1) > 0)");
        }

        return costCenterRepository.getListCostCenter(query);
    }

}
