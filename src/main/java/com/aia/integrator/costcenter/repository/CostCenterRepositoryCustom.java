package com.aia.integrator.costcenter.repository;

import com.aia.integrator.costcenter.domain.CostCenter;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CostCenterRepositoryCustom {

    List<CostCenter> getListCostCenter(StringBuilder param);

}
