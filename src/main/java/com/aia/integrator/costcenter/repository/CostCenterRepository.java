package com.aia.integrator.costcenter.repository;

import com.aia.integrator.costcenter.domain.CostCenter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CostCenterRepository extends CostCenterRepositoryCustom, CrudRepository<CostCenter, Long> {

}
