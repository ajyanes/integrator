package com.aia.integrator.costcenter.repository.impl;

import com.aia.integrator.costcenter.domain.CostCenter;
import com.aia.integrator.costcenter.repository.CostCenterRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
@PropertySource("classpath:sentences.xml")
public class CostCenterRepositoryCustomImpl implements CostCenterRepositoryCustom {

    @Autowired
    private EntityManager em;

    @Autowired
    private Environment env;

    @Override
    public List<CostCenter> getListCostCenter(StringBuilder param) {
        String query = env.getProperty("showCostCenter.sql");
        query += param;
        Query nativeQuery = em.createNativeQuery(query, CostCenter.class);
        nativeQuery.setHint("org.hibernate.fetchSize",1000);

        return nativeQuery.getResultList();
    }

}
