package com.aia.integrator.costcenter.domain;

import javax.persistence.IdClass;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
@IdClass(value = CostCenterPk.class)
public class CostCenter implements Serializable {

    @Id
    @Column(name = "igecompania")
    private String companycenter;
    @Id
    @Column(name = "igecodigo")
    private String codecenter;
    @Column(name = "igenomblarg")
    private String namecenter;
    @Column(name = "company_name")
    private String namecompany;
    @Column(name = "parvalor")
    private String codecompany;

}
