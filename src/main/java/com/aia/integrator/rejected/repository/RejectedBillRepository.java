package com.aia.integrator.rejected.repository;

import com.aia.integrator.rejected.domain.RejectedBill;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RejectedBillRepository extends RejectedBillRepositoryCustom, CrudRepository<RejectedBill, Long> {

}
