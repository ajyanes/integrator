package com.aia.integrator.rejected.repository;

import com.aia.integrator.rejected.domain.RejectedBill;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface RejectedBillRepositoryCustom {


  List<RejectedBill> getRejected(StringBuilder param);
}
