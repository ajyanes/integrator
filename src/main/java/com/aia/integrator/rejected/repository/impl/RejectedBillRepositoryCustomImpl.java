package com.aia.integrator.rejected.repository.impl;

import com.aia.integrator.provider.domain.Provider;
import com.aia.integrator.rejected.domain.RejectedBill;
import com.aia.integrator.rejected.repository.RejectedBillRepositoryCustom;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

@Repository
@PropertySource("classpath:sentences.xml")
public class RejectedBillRepositoryCustomImpl implements RejectedBillRepositoryCustom {

  @Autowired
  private EntityManager em;

  @Autowired
  private Environment env;


  @Override
  public List<RejectedBill> getRejected(StringBuilder param) {
    String query = env.getProperty("showRejected.sql");
    query += param;
    Query nativeQuery = em.createNativeQuery(query, RejectedBill.class);
    nativeQuery.setHint("org.hibernate.fetchSize",1000);

    return nativeQuery.getResultList();
  }
}
