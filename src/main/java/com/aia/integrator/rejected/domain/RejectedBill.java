package com.aia.integrator.rejected.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "FACTURAS_RECHAZADAS")
public class RejectedBill  implements Serializable {

  @Id
  @Column(name = "FACTURA")
  private String billId;
  @Column(name = "NIT")
  private String providerId;
  @Column(name = "NOMBRETERCERO")
  private String providername;
  @Column(name = "FECHAFACTURA")
  private LocalDate dateBill;
  @Column(name = "FECHAEXPEDICION")
  private LocalDate expeditionDate;
  @Column(name = "VALOR")
  private Long valueBill;
  @Column(name = "MOTIVO")
  private String reason;
  @Column(name = "ESTADO")
  private String state;
  @Column(name = "PREFIJO")
  private String prefix;
}
