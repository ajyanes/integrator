package com.aia.integrator.rejected.service;


import com.aia.integrator.rejected.domain.RejectedBill;
import java.util.List;

public interface RejectedBillService {

  RejectedBill saveRejected(RejectedBill rejectedBill);

  List<RejectedBill> getRejected(String bill, String provider, String state);

}
