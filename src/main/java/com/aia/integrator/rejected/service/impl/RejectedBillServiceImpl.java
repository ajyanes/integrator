package com.aia.integrator.rejected.service.impl;

import com.aia.integrator.rejected.domain.RejectedBill;
import com.aia.integrator.rejected.repository.RejectedBillRepository;
import com.aia.integrator.rejected.service.RejectedBillService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RejectedBillServiceImpl implements RejectedBillService {


  @Autowired
  private RejectedBillRepository rejectedBillRepository;

  @Override
  public RejectedBill saveRejected(RejectedBill rejectedBill) {
    return rejectedBillRepository.save(rejectedBill);
  }

  @Override
  public List<RejectedBill> getRejected(String bill, String provider, String state) {
    StringBuilder query = new StringBuilder();

    if (!bill.equals("")) {
      query.append(" AND FACTURA = '" + bill+"'");
    }

    if (!provider.equals("")) {
      query.append(" AND NIT ='" + provider + "'");
    }

    if (!state.equals("")) {
      query.append(" AND ESTADO ='" + state + "'");
    }

    return rejectedBillRepository.getRejected(query);
  }
}
