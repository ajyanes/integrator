package com.aia.integrator.provider.service.impl;

import com.aia.integrator.provider.domain.Provider;
import com.aia.integrator.provider.repository.ProviderRepository;
import com.aia.integrator.provider.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProviderServiceImpl implements ProviderService {

    private final String addQuery = "and exists (select '1' from un_terceprove p "
        + "where tprtercegener = tgecodigo and tprcompania = tgecompania and p.eobcodigo = 'AC') "
        + "order by tgecompania";

    @Autowired
    private ProviderRepository providerRepository;

    @Override
    public List<Provider> getProvider(String cod, String name, String company) {
        StringBuilder query = new StringBuilder();

        if (!company.equals("")) {
            query.append(" and tgecompania = '" + company+"'");
        }

        if (!cod.equals("")) {
            query.append(" and (CHARINDEX('"+cod+"',tgecodigo,1) > 0 or CHARINDEX('"+cod+"',tgenombcomp,1) > 0)");
        }

        if (!name.equals("")) {
            query.append(" and (CHARINDEX('"+name+"',tgecodigo,1) > 0 or CHARINDEX('"+name+"',tgenombcomp,1) > 0)");
        }

        query.append(addQuery);

        return providerRepository.getProvider(query);
    }
}
