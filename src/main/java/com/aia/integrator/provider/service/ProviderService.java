package com.aia.integrator.provider.service;

import com.aia.integrator.provider.domain.Provider;

import java.util.List;

public interface ProviderService {

    List<Provider> getProvider(String cod, String name, String company);

}
