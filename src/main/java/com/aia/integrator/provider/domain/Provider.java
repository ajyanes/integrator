package com.aia.integrator.provider.domain;

import javax.persistence.IdClass;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
@IdClass(value = ProviderPk.class)
public class Provider implements Serializable {
    @Id
    @Column(name = "tgecompania")
    private String companyProvider;
    @Id
    @Column(name = "tgecodigo")
    private String codeProvider;
    @Column(name = "tgenombcomp")
    private String nameProvider;
    @Column(name = "company_name")
    private String namecompany;
    @Column(name = "parvalor")
    private String codecompany;
}
