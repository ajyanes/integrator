package com.aia.integrator.provider.repository;

import com.aia.integrator.provider.domain.Provider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProviderRepository extends ProviderRepositoryCustom, CrudRepository<Provider, Long> {

}
