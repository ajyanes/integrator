package com.aia.integrator.provider.repository.impl;

import com.aia.integrator.provider.domain.Provider;
import com.aia.integrator.provider.repository.ProviderRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
@PropertySource("classpath:sentences.xml")
public class ProviderRepositoryCustomImpl implements ProviderRepositoryCustom {

    @Autowired
    private EntityManager em;

    @Autowired
    private Environment env;

    @Override
    public List<Provider> getProvider(StringBuilder qry) {
        String query = env.getProperty("showProviders.sql");
        query += qry;
        Query nativeQuery = em.createNativeQuery(query, Provider.class);
        nativeQuery.setHint("org.hibernate.fetchSize",1000);

        return nativeQuery.getResultList();
    }

}
