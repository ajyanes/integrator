package com.aia.integrator.provider.repository;

import com.aia.integrator.provider.domain.Provider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProviderRepositoryCustom {

    List<Provider> getProvider(StringBuilder param);

}
