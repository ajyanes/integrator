FROM adoptopenjdk/openjdk8:latest
RUN mkdir /opt/app
COPY /target/integrator-0.0.1-SNAPSHOT.jar /integrator-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "/integrator-0.0.1-SNAPSHOT.jar"]
EXPOSE 9191
